#
# SPDX-FileCopyrightText: 2010-2022, Gilles Caulier, <caulier dot gilles at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
#

KDOCTOOLS_CREATE_HANDBOOK(index.docbook INSTALL_DESTINATION ${HTML_INSTALL_DIR}/en SUBDIR digikam)
