<sect3 id="using-setup-raw">
    <title>RAW Decoding Settings</title>

    <para>
        In the early versions of &digikam; the Image Editor was just a viewer for photographs, but it is rapidly developing into a very useful photo manipulation tool. This dialog allows you to control how the Image Editor will behave when opening RAW files.
    </para>

    <para>
        <inlinemediaobject><imageobject>
            <imagedata fileref="&path;using-setup-editor-raw-behavior.png" format="PNG" />
        </imageobject></inlinemediaobject>
    </para>

    <para>
        <guilabel>Fast and simple, as 8 bit image</guilabel>
    </para>

    <para>
        RAW files will be decoded to 8-bit color depth with a BT.709 gamma curve and a 99th-percentile white point. This mode is faster than 16-bit decoding. In 8-bit mode only the <guilabel>Auto Brightness</guilabel> setting will be taken into account (dcraw limitation).
    </para>
    
    <para>
        <guilabel>Use the default settings, in 16 bit</guilabel>
    </para>
    
    <para>
        If enabled, all RAW files will be decoded to 16-bit color depth using a linear gamma curve and according to the settings in the <guilabel>RAW Default Settings</guilabel> tab. To prevent dark image rendering in the editor, it is recommended to use Color Management in this mode.
    </para>
    
    <para>
        <guilabel>Always open the Raw Import Tool to customize settings</guilabel>
    </para>
    
    <para>
        With this option checked the Raw Import Tool will open at the Right Side Bar in the Image Editor so that you can set individual parameters for every image you open.
    </para>
    
</sect3>

<!--
Local Variables:
mode: sgml
sgml-omittag: nil
sgml-shorttag: t
End:
-->
